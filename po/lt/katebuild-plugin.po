# Lithuanian translations for katebuild-plugin package.
# This file is distributed under the same license as the katebuild-plugin package.
#
# Andrius Štikonas <andrius@stikonas.eu>, 2009.
# Remigijus Jarmalavičius <remigijus@jarmalavicius.lt>, 2011, 2012.
# Liudas Alisauskas <liudas@akmc.lt>, 2013.
# Liudas Ališauskas <liudas@aksioma.lt>, 2014.
# Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: katebuild-plugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-18 02:00+0000\n"
"PO-Revision-Date: 2015-12-29 21:20+0200\n"
"Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>\n"
"Language-Team: lt <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Lokalize 1.5\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "Išvestis"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr "Kurti dar kartą"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr "Atšaukti"

#: buildconfig.cpp:26
#, kde-format
msgid "Add errors and warnings to Diagnostics"
msgstr ""

#: buildconfig.cpp:37
#, fuzzy, kde-format
#| msgid "Build again"
msgid "Build & Run"
msgstr "Kurti dar kartą"

#: buildconfig.cpp:43
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Build & Run Settings"
msgstr "Kompiliavimas nepavyko."

#: plugin_katebuild.cpp:212 plugin_katebuild.cpp:219 plugin_katebuild.cpp:1221
#, kde-format
msgid "Build"
msgstr "Kompiliuoti"

#: plugin_katebuild.cpp:222
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Select Target..."
msgstr "Kompiliavimas nepavyko."

#: plugin_katebuild.cpp:227
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Build Selected Target"
msgstr "Kompiliavimas nepavyko."

#: plugin_katebuild.cpp:232
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Build and Run Selected Target"
msgstr "Kompiliavimas nepavyko."

#: plugin_katebuild.cpp:237
#, kde-format
msgid "Stop"
msgstr "Sustabdyti"

#: plugin_katebuild.cpp:242
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr ""

#: plugin_katebuild.cpp:262
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr ""

#: plugin_katebuild.cpp:284
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "Tikslo nuostatos"

#: plugin_katebuild.cpp:403
#, fuzzy, kde-format
#| msgid "Build again"
msgid "Build Information"
msgstr "Kurti dar kartą"

#: plugin_katebuild.cpp:619
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "Nėra failo arba aplanko nurodyto statymui."

#: plugin_katebuild.cpp:623
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr ""
"Failas „%1“ nėra vietinis failas. Ne vietiniai failai negali būti "
"kompiliuojami."

#: plugin_katebuild.cpp:670
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""

#: plugin_katebuild.cpp:684
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "Nepavyko paleisti „%1“. Išėjimo statusas (exitStatus) = %2"

#: plugin_katebuild.cpp:699
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr ""

#: plugin_katebuild.cpp:806
#, kde-format
msgid "No target available for building."
msgstr ""

#: plugin_katebuild.cpp:820
#, fuzzy, kde-format
#| msgid "There is no file or directory specified for building."
msgid "There is no local file or directory specified for building."
msgstr "Nėra failo arba aplanko nurodyto statymui."

#: plugin_katebuild.cpp:826
#, kde-format
msgid "Already building..."
msgstr ""

#: plugin_katebuild.cpp:853
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Building target <b>%1</b> ..."
msgstr "Kompiliavimas nepavyko."

#: plugin_katebuild.cpp:867
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr ""

#: plugin_katebuild.cpp:903
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:909
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "Rasta viena klaida."
msgstr[1] "Rastos %1 klaidos."
msgstr[2] "Rasta %1 klaidų."
msgstr[3] "Rasta %1 klaida."

#: plugin_katebuild.cpp:913
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "Rastas vienas įspėjimas."
msgstr[1] "Rasti %1 įspėjimai."
msgstr[2] "Rasta %1 įspėjimų."
msgstr[3] "Rastas %1 įspėjimas."

#: plugin_katebuild.cpp:916
#, fuzzy, kde-format
#| msgid "Found one error."
#| msgid_plural "Found %1 errors."
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "Rasta viena klaida."
msgstr[1] "Rastos %1 klaidos."
msgstr[2] "Rasta %1 klaidų."
msgstr[3] "Rasta %1 klaida."

#: plugin_katebuild.cpp:921
#, kde-format
msgid "Build failed."
msgstr "Kompiliavimas nepavyko."

#: plugin_katebuild.cpp:923
#, kde-format
msgid "Build completed without problems."
msgstr "Kompiliavimas pavyko be problemų."

#: plugin_katebuild.cpp:928
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:952
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr ""

#: plugin_katebuild.cpp:1178
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark an error."
#| msgid "error"
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "klaida"

#: plugin_katebuild.cpp:1181
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark a warning."
#| msgid "warning"
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "perspėjimas"

#: plugin_katebuild.cpp:1184
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr ""

#: plugin_katebuild.cpp:1187
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "neapibrėžta nuoroda"

#: plugin_katebuild.cpp:1220 TargetModel.cpp:285 TargetModel.cpp:297
#, fuzzy, kde-format
#| msgid "Targets"
msgid "Target Set"
msgstr "Tikslas"

#: plugin_katebuild.cpp:1222
#, kde-format
msgid "Clean"
msgstr "Valyti"

#: plugin_katebuild.cpp:1223
#, kde-format
msgid "Config"
msgstr "Konfigūracija"

#: plugin_katebuild.cpp:1224
#, fuzzy, kde-format
#| msgid "Config"
msgid "ConfigClean"
msgstr "Konfigūracija"

#: plugin_katebuild.cpp:1415
#, kde-format
msgid "Cannot save build targets in: %1"
msgstr ""

#: TargetHtmlDelegate.cpp:50
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr ""

#: TargetHtmlDelegate.cpp:52
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr ""

#: TargetHtmlDelegate.cpp:101
#, fuzzy, kde-format
#| msgid "Leave empty to use the directory of the current document. "
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr "Palikite tuščią, kad būtų panaudotas dabartinio dokumento aplankas."

#: TargetHtmlDelegate.cpp:105
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"Naudoti:\n"
"„%f“ dabartiniam failui\n"
"„%d“ katalogui dabartinio failo\n"
"„%n“ dabartiniam failui be priešdėlio"

#: TargetModel.cpp:530
#, kde-format
msgid "Project"
msgstr ""

#: TargetModel.cpp:530
#, kde-format
msgid "Session"
msgstr ""

#: TargetModel.cpp:624
#, kde-format
msgid "Command/Target-set Name"
msgstr ""

#: TargetModel.cpp:627
#, fuzzy, kde-format
#| msgid "Working directory"
msgid "Working Directory / Command"
msgstr "Darbinis katalogas"

#: TargetModel.cpp:630
#, kde-format
msgid "Run Command"
msgstr ""

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr ""

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr ""

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr ""

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "Pridėti naują tikslą"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr ""

#: targets.cpp:48
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Build and run selected target"
msgstr "Kompiliavimas nepavyko."

#: targets.cpp:52
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Move selected target up"
msgstr "Kompiliavimas nepavyko."

#: targets.cpp:56
#, kde-format
msgid "Move selected target down"
msgstr ""

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "&Kompiliuoti"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr ""

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr ""

#, fuzzy
#~| msgid "Project Plugin Target"
#~ msgid "Project Plugin Targets"
#~ msgstr "Projekto papildinio tikslas"

#, fuzzy
#~| msgid "Build"
#~ msgid "build"
#~ msgstr "Kompiliuoti"

#, fuzzy
#~| msgid "Clean"
#~ msgid "clean"
#~ msgstr "Valyti"

#, fuzzy
#~| msgid "Build failed."
#~ msgid "Building <b>%1</b> had warnings."
#~ msgstr "Kompiliavimas nepavyko."

#~ msgid "Show:"
#~ msgstr "Rodyti:"

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "Failas"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "Eilutė"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "Pranešimas"

#~ msgid "Next Error"
#~ msgstr "Kita klaida"

#~ msgid "Previous Error"
#~ msgstr "Ankstesnė klaida"

#, fuzzy
#~| msgctxt "The same word as 'make' uses to mark an error."
#~| msgid "error"
#~ msgid "Error"
#~ msgstr "klaida"

#, fuzzy
#~| msgctxt "The same word as 'make' uses to mark a warning."
#~| msgid "warning"
#~ msgid "Warning"
#~ msgstr "perspėjimas"

#, fuzzy
#~| msgid "Errors"
#~ msgid "Only Errors"
#~ msgstr "Klaidos"

#~ msgid "Errors and Warnings"
#~ msgstr "Klaidos ir pranešimai"

#~ msgid "Parsed Output"
#~ msgstr "Apdorota išvestis"

#~ msgid "Full Output"
#~ msgstr "Pilna išvestis"

#, fuzzy
#~| msgid "Build failed."
#~ msgid "Build and Run Default Target"
#~ msgstr "Kompiliavimas nepavyko."

#, fuzzy
#~| msgid "Build failed."
#~ msgid "Build Previous Target"
#~ msgstr "Kompiliavimas nepavyko."

#, fuzzy
#~| msgid "Config"
#~ msgid "config"
#~ msgstr "Konfigūracija"

#, fuzzy
#~| msgid "Build Plugin"
#~ msgid "Kate Build Plugin"
#~ msgstr "Kompiliavimo papildinys"

#~ msgid "Build Output"
#~ msgstr "Kompiliavimo išvestis"
