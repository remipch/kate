# Lithuanian translations for trunk-kf package.
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the trunk-kf package.
#
# Automatically generated, 2014.
# Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: trunk-kf 5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-13 01:39+0000\n"
"PO-Revision-Date: 2017-10-02 10:25+0200\n"
"Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#: katefiletree.cpp:120
#, kde-format
msgctxt "@action:inmenu"
msgid "Reloa&d"
msgstr "Įkelti iš &naujo"

#: katefiletree.cpp:122
#, kde-format
msgid "Reload selected document(s) from disk."
msgstr "Įkelti iš naujo pažymėtą dokumentą(-us) iš disko."

#: katefiletree.cpp:124
#, kde-format
msgctxt "@action:inmenu"
msgid "Close"
msgstr "Užverti"

#: katefiletree.cpp:126
#, kde-format
msgid "Close the current document."
msgstr "Užverti dabartinį dokumentą."

#: katefiletree.cpp:128
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Expand recursively"
msgctxt "@action:inmenu"
msgid "Expand Recursively"
msgstr "Išskleisti rekursyviai"

#: katefiletree.cpp:130
#, kde-format
msgid "Expand the file list sub tree recursively."
msgstr ""

#: katefiletree.cpp:132
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Collapse recursively"
msgctxt "@action:inmenu"
msgid "Collapse Recursively"
msgstr "Suskleisti rekursyviai"

#: katefiletree.cpp:134
#, kde-format
msgid "Collapse the file list sub tree recursively."
msgstr ""

#: katefiletree.cpp:136
#, kde-format
msgctxt "@action:inmenu"
msgid "Close Other"
msgstr "Užverti kitus"

#: katefiletree.cpp:138
#, kde-format
msgid "Close other documents in this folder."
msgstr "Užverti kitus dokumentus šiame aplanke."

#: katefiletree.cpp:141
#, fuzzy, kde-format
#| msgid "Opening Order"
msgctxt "@action:inmenu"
msgid "Open Containing Folder"
msgstr "Atvėrimo tvarka"

#: katefiletree.cpp:143
#, kde-format
msgid "Open the folder this file is located in."
msgstr ""

#: katefiletree.cpp:145
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy Location"
msgstr ""

#: katefiletree.cpp:147
#, fuzzy, kde-format
#| msgid "Copy the filename of the file."
msgid "Copy path and filename to the clipboard."
msgstr "Kopijuoti vardą dabar redaguojamo failo."

#: katefiletree.cpp:149
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Rename File"
msgctxt "@action:inmenu"
msgid "Rename..."
msgstr "Pervadinti failą"

#: katefiletree.cpp:151
#, kde-format
msgid "Rename the selected file."
msgstr "Pervadinti šį pažymėtą failą."

#: katefiletree.cpp:154
#, kde-format
msgid "Print selected document."
msgstr "Spausdinti pažymėtą dokumentą."

#: katefiletree.cpp:157
#, kde-format
msgid "Show print preview of current document"
msgstr "Rodyti dabartinio dokumento spausdinimo peržiūrą"

#: katefiletree.cpp:159
#, fuzzy, kde-format
#| msgid "Delete file?"
msgctxt "@action:inmenu"
msgid "Delete"
msgstr "Pašalinti failą?"

#: katefiletree.cpp:161
#, kde-format
msgid "Close and delete selected file from storage."
msgstr "Uždaryti ir ištrinti pažymėtą failą iš talpyklos."

#: katefiletree.cpp:165
#, kde-format
msgctxt "@action:inmenu"
msgid "Clear History"
msgstr "Išvalyti istoriją"

#: katefiletree.cpp:167
#, kde-format
msgid "Clear edit/view history."
msgstr "Išvalyti redagavimo/peržiūros istoriją."

#: katefiletree.cpp:230
#, kde-format
msgctxt "@action:inmenu"
msgid "Tree Mode"
msgstr "Medžio veiksena"

#: katefiletree.cpp:231
#, kde-format
msgid "Set view style to Tree Mode"
msgstr "Nustatyti rodinio stilių į medžio veikseną"

#: katefiletree.cpp:237
#, kde-format
msgctxt "@action:inmenu"
msgid "List Mode"
msgstr "Sąrašo veiksena"

#: katefiletree.cpp:238
#, kde-format
msgid "Set view style to List Mode"
msgstr "Nustatyti rodinio stilių į sąrašo veikseną"

#: katefiletree.cpp:245
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Document Name"
msgstr "Dokumento vardas"

#: katefiletree.cpp:246
#, kde-format
msgid "Sort by Document Name"
msgstr "Rikiuoti pagal dokumento pavadinimą"

#: katefiletree.cpp:251
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Document Path"
msgstr "Dokumento kelias"

#: katefiletree.cpp:251
#, kde-format
msgid "Sort by Document Path"
msgstr "Rikiuoti pagal dokumento kelią"

#: katefiletree.cpp:255
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Opening Order"
msgstr "Atvėrimo tvarka"

#: katefiletree.cpp:256
#, kde-format
msgid "Sort by Opening Order"
msgstr "Rikiuoti pagal atvėrimo tvarką"

#: katefiletree.cpp:259 katefiletreeconfigpage.cpp:73
#, kde-format
msgid "Custom Sorting"
msgstr ""

#: katefiletree.cpp:388
#, kde-format
msgctxt "@action:inmenu"
msgid "Open With"
msgstr "Atverti su"

#: katefiletree.cpp:402
#, kde-format
msgid "Show File Git History"
msgstr ""

#: katefiletree.cpp:441
#, kde-format
msgctxt "@action:inmenu"
msgid "View Mode"
msgstr "Rodymo būdas"

#: katefiletree.cpp:445
#, kde-format
msgctxt "@action:inmenu"
msgid "Sort By"
msgstr "Rikiuoti pagal"

#: katefiletreeconfigpage.cpp:44
#, kde-format
msgid "Background Shading"
msgstr "Fono šešėliai"

#: katefiletreeconfigpage.cpp:51
#, kde-format
msgid "&Viewed documents' shade:"
msgstr "&Peržiūrimo dokumento šešėlis:"

#: katefiletreeconfigpage.cpp:57
#, kde-format
msgid "&Modified documents' shade:"
msgstr "&Pakeisto dokumento šešėlis:"

#: katefiletreeconfigpage.cpp:65
#, kde-format
msgid "&Sort by:"
msgstr "&Rikiuoti pagal:"

#: katefiletreeconfigpage.cpp:70
#, kde-format
msgid "Opening Order"
msgstr "Atvėrimo tvarka"

#: katefiletreeconfigpage.cpp:71
#, kde-format
msgid "Document Name"
msgstr "Dokumento vardas"

#: katefiletreeconfigpage.cpp:72
#, kde-format
msgid "Url"
msgstr "Url"

#: katefiletreeconfigpage.cpp:78
#, kde-format
msgid "&View Mode:"
msgstr "&Rodymo būdas:"

#: katefiletreeconfigpage.cpp:83
#, kde-format
msgid "Tree View"
msgstr "Medžio vaizdas"

#: katefiletreeconfigpage.cpp:84
#, kde-format
msgid "List View"
msgstr "Sąrašo vaizdas"

#: katefiletreeconfigpage.cpp:89
#, kde-format
msgid "&Show Full Path"
msgstr "Rodyti pilną &kelią"

#: katefiletreeconfigpage.cpp:94
#, kde-format
msgid "Show &Toolbar"
msgstr ""

#: katefiletreeconfigpage.cpp:97
#, kde-format
msgid "Show Close Button"
msgstr ""

#: katefiletreeconfigpage.cpp:99
#, kde-format
msgid ""
"When enabled, this will show a close button for opened documents on hover."
msgstr ""

#: katefiletreeconfigpage.cpp:104
#, kde-format
msgid ""
"When background shading is enabled, documents that have been viewed or "
"edited within the current session will have a shaded background. The most "
"recent documents have the strongest shade."
msgstr ""
"Įgalinus fono šešėlius, dokumentai, kurie buvo peržiūrimi ar redaguojami "
"einamojoje sesijoje, bus rodomi su šešėliu fone. naujausių dokumentų šešėlis "
"bus stipriausias."

#: katefiletreeconfigpage.cpp:107
#, kde-format
msgid "Set the color for shading viewed documents."
msgstr "Nurodyti spalvą peržiūrimų dokumentų šešėliams."

#: katefiletreeconfigpage.cpp:109
#, kde-format
msgid ""
"Set the color for modified documents. This color is blended into the color "
"for viewed files. The most recently edited documents get most of this color."
msgstr ""
"Nurodyti pakeistų dokumentų spalvą. Ši spalva bus įlieta į peržiūrimų "
"dokumentų spalvą. Dokumentai, kurie buvo redaguoti paskutiniai, turės "
"daugiausiai šio atspalvio."

#: katefiletreeconfigpage.cpp:114
#, kde-format
msgid ""
"When enabled, in tree mode, top level folders will show up with their full "
"path rather than just the last folder name."
msgstr ""
"Įjungus šią parinktį, medžio veiksenoje, viršutinio lygmens aplankai bus "
"rodomi su pilnu keliu, o ne tik aplanko pavadinimu."

#: katefiletreeconfigpage.cpp:117
#, kde-format
msgid ""
"When enabled, a toolbar with actions like “Save” are displayed above the "
"list of documents."
msgstr ""

#: katefiletreeconfigpage.cpp:136 katefiletreeplugin.cpp:127
#: katefiletreeplugin.cpp:134
#, kde-format
msgid "Documents"
msgstr "Dokumentai"

#: katefiletreeconfigpage.cpp:141
#, kde-format
msgid "Configure Documents"
msgstr "Konfigūruoti dokumentus"

#: katefiletreemodel.cpp:494
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Open With"
msgctxt ""
"Open here is a description, i.e. 'list of widgets that are open' not a verb"
msgid "Open Widgets"
msgstr "Atverti su"

#: katefiletreemodel.cpp:644
#, kde-format
msgctxt "%1 is the full path"
msgid ""
"<p><b>%1</b></p><p>The document has been modified by another application.</p>"
msgstr "<p><b>%1</b></p><p>Dokumentas buvo pakeistas kitos programos.</p>"

#: katefiletreeplugin.cpp:248
#, kde-format
msgid "Previous Document"
msgstr "Ankstesnis dokumentas"

#: katefiletreeplugin.cpp:254
#, kde-format
msgid "Next Document"
msgstr "Kitas dokumentas"

#: katefiletreeplugin.cpp:260
#, fuzzy, kde-format
#| msgid "&Show Active"
msgid "&Show Active Document"
msgstr "Rodyti &aktyvius"

#: katefiletreeplugin.cpp:265
#, kde-format
msgid "Save"
msgstr ""

#: katefiletreeplugin.cpp:266
#, kde-format
msgid "Save the current document"
msgstr "Įrašyti esamą dokumentą"

#: katefiletreeplugin.cpp:270
#, kde-format
msgid "Save As"
msgstr ""

#: katefiletreeplugin.cpp:271
#, fuzzy, kde-format
#| msgid "Save current document under new name"
msgid "Save the current document under a new name"
msgstr "Įrašyti esamą dokumentą nauju pavadinimu"

#. i18n: ectx: Menu (go)
#: ui.rc:7
#, kde-format
msgid "&Go"
msgstr ""

#~ msgid "&Other..."
#~ msgstr "&Kita..."

#~ msgid "Do you really want to delete file \"%1\" from storage?"
#~ msgstr "Ar tikrai norite pašalinti failą „%1“ iš laikmenos?"

#~ msgid "Delete file?"
#~ msgstr "Pašalinti failą?"

#~ msgid "File \"%1\" could not be deleted."
#~ msgstr "Nepavyko ištrinti failo „%1“."

#~ msgid "Rename file"
#~ msgstr "Pervadinti failą"

#~ msgid "New file name"
#~ msgstr "Naujas failo pavadinimas"

#~ msgid "File \"%1\" could not be moved to \"%2\""
#~ msgstr "Negalima „%1“ perkelti į „%2“"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Liudas Ališauskas"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "liudas@aksioma.lt"

#, fuzzy
#~| msgctxt "@action:inmenu"
#~| msgid "Close Other"
#~ msgid "Close Widget"
#~ msgstr "Užverti kitus"

#~ msgid "Kate File Tree"
#~ msgstr "Kate Failų medis"

#, fuzzy
#~| msgctxt "@action:inmenu"
#~| msgid "Copy Filename"
#~ msgctxt "@action:inmenu"
#~ msgid "Copy File Path"
#~ msgstr "Kopijuoti failo vardą"

#~ msgid "&View"
#~ msgstr "&Rodymas"

#~ msgid "Save Current Document"
#~ msgstr "Įrašyti esamą dokumentą"

#~ msgid "Save Current Document As"
#~ msgstr "Įrašyti esamą dokumentą kaip"

#~ msgid "Application '%1' not found."
#~ msgstr "Programa „%1“ nerasta."

#~ msgid "Application not found"
#~ msgstr "Programa nerasta"

#~ msgctxt "@action:inmenu"
#~ msgid "Delete Document"
#~ msgstr "Ištrinti dokumentą"
