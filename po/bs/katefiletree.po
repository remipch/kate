# Bosnian translations for PACKAGE package
# engleski prevodi za paket PACKAGE.
# Copyright (C) 2015 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Samir ribic <megaribi@localhost>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: kde5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-13 01:39+0000\n"
"PO-Revision-Date: 2015-02-24 19:40+0100\n"
"Last-Translator: Samir Ribić <megaribi@epn.ba>\n"
"Language-Team: Bosnian\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: katefiletree.cpp:120
#, kde-format
msgctxt "@action:inmenu"
msgid "Reloa&d"
msgstr "&Učitaj ponovo"

#: katefiletree.cpp:122
#, kde-format
msgid "Reload selected document(s) from disk."
msgstr ""

#: katefiletree.cpp:124
#, kde-format
msgctxt "@action:inmenu"
msgid "Close"
msgstr "Zatvori"

#: katefiletree.cpp:126
#, kde-format
msgid "Close the current document."
msgstr "Zatvori tekući dokument."

#: katefiletree.cpp:128
#, kde-format
msgctxt "@action:inmenu"
msgid "Expand Recursively"
msgstr ""

#: katefiletree.cpp:130
#, kde-format
msgid "Expand the file list sub tree recursively."
msgstr ""

#: katefiletree.cpp:132
#, kde-format
msgctxt "@action:inmenu"
msgid "Collapse Recursively"
msgstr ""

#: katefiletree.cpp:134
#, kde-format
msgid "Collapse the file list sub tree recursively."
msgstr ""

#: katefiletree.cpp:136
#, kde-format
msgctxt "@action:inmenu"
msgid "Close Other"
msgstr "Zatvori druge"

#: katefiletree.cpp:138
#, kde-format
msgid "Close other documents in this folder."
msgstr ""

#: katefiletree.cpp:141
#, fuzzy, kde-format
#| msgid "Opening Order"
msgctxt "@action:inmenu"
msgid "Open Containing Folder"
msgstr "redoslijedu otvaranja"

#: katefiletree.cpp:143
#, kde-format
msgid "Open the folder this file is located in."
msgstr ""

#: katefiletree.cpp:145
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy Location"
msgstr ""

#: katefiletree.cpp:147
#, fuzzy, kde-format
#| msgid "Copy the filename of the file."
msgid "Copy path and filename to the clipboard."
msgstr "Kopiraj ime datoteke za tu datoteku"

#: katefiletree.cpp:149
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Rename File"
msgctxt "@action:inmenu"
msgid "Rename..."
msgstr "Preimenuj datoteku"

#: katefiletree.cpp:151
#, kde-format
msgid "Rename the selected file."
msgstr ""

#: katefiletree.cpp:154
#, kde-format
msgid "Print selected document."
msgstr ""

#: katefiletree.cpp:157
#, kde-format
msgid "Show print preview of current document"
msgstr ""

#: katefiletree.cpp:159
#, fuzzy, kde-format
#| msgid "Delete file?"
msgctxt "@action:inmenu"
msgid "Delete"
msgstr "Obrisati datoteku?"

#: katefiletree.cpp:161
#, kde-format
msgid "Close and delete selected file from storage."
msgstr ""

#: katefiletree.cpp:165
#, kde-format
msgctxt "@action:inmenu"
msgid "Clear History"
msgstr "Očisti historijat"

#: katefiletree.cpp:167
#, kde-format
msgid "Clear edit/view history."
msgstr ""

#: katefiletree.cpp:230
#, kde-format
msgctxt "@action:inmenu"
msgid "Tree Mode"
msgstr "Režim stabla"

#: katefiletree.cpp:231
#, kde-format
msgid "Set view style to Tree Mode"
msgstr "Postavi stil pogleda na režim stabla"

#: katefiletree.cpp:237
#, kde-format
msgctxt "@action:inmenu"
msgid "List Mode"
msgstr "Režim spiska"

#: katefiletree.cpp:238
#, kde-format
msgid "Set view style to List Mode"
msgstr "Postavi stil pogleda na režim spiska"

#: katefiletree.cpp:245
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Document Name"
msgstr "imenu dokumenta"

#: katefiletree.cpp:246
#, kde-format
msgid "Sort by Document Name"
msgstr "Sortiraj po imenu dokumenta"

#: katefiletree.cpp:251
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Document Path"
msgstr "Staza dokumenta"

#: katefiletree.cpp:251
#, kde-format
msgid "Sort by Document Path"
msgstr "Sortiraj po stazi dokumenta"

#: katefiletree.cpp:255
#, kde-format
msgctxt "@action:inmenu sorting option"
msgid "Opening Order"
msgstr "Redoslijedu otvaranja"

#: katefiletree.cpp:256
#, kde-format
msgid "Sort by Opening Order"
msgstr "Sortiraj po redoslijedu otvaranja"

#: katefiletree.cpp:259 katefiletreeconfigpage.cpp:73
#, kde-format
msgid "Custom Sorting"
msgstr ""

#: katefiletree.cpp:388
#, kde-format
msgctxt "@action:inmenu"
msgid "Open With"
msgstr "Otvori pomoću"

#: katefiletree.cpp:402
#, kde-format
msgid "Show File Git History"
msgstr ""

#: katefiletree.cpp:441
#, kde-format
msgctxt "@action:inmenu"
msgid "View Mode"
msgstr "Režim prikaza"

#: katefiletree.cpp:445
#, kde-format
msgctxt "@action:inmenu"
msgid "Sort By"
msgstr "Sortiraj po"

#: katefiletreeconfigpage.cpp:44
#, kde-format
msgid "Background Shading"
msgstr "Sijenčenje pozadine"

#: katefiletreeconfigpage.cpp:51
#, kde-format
msgid "&Viewed documents' shade:"
msgstr "Sijenka &pogledanih dokumenata:"

#: katefiletreeconfigpage.cpp:57
#, kde-format
msgid "&Modified documents' shade:"
msgstr "Sijenka &izmijenjenih dokumenata:"

#: katefiletreeconfigpage.cpp:65
#, kde-format
msgid "&Sort by:"
msgstr "&Poređaj po:"

#: katefiletreeconfigpage.cpp:70
#, kde-format
msgid "Opening Order"
msgstr "redoslijedu otvaranja"

#: katefiletreeconfigpage.cpp:71
#, kde-format
msgid "Document Name"
msgstr "imenu dokumenta"

#: katefiletreeconfigpage.cpp:72
#, kde-format
msgid "Url"
msgstr "Url"

#: katefiletreeconfigpage.cpp:78
#, kde-format
msgid "&View Mode:"
msgstr "Način &pogleda:"

#: katefiletreeconfigpage.cpp:83
#, kde-format
msgid "Tree View"
msgstr "Prikaz stabla"

#: katefiletreeconfigpage.cpp:84
#, kde-format
msgid "List View"
msgstr "Pregled liste"

#: katefiletreeconfigpage.cpp:89
#, kde-format
msgid "&Show Full Path"
msgstr "Prikaži punu &stazu"

#: katefiletreeconfigpage.cpp:94
#, kde-format
msgid "Show &Toolbar"
msgstr ""

#: katefiletreeconfigpage.cpp:97
#, kde-format
msgid "Show Close Button"
msgstr ""

#: katefiletreeconfigpage.cpp:99
#, kde-format
msgid ""
"When enabled, this will show a close button for opened documents on hover."
msgstr ""

#: katefiletreeconfigpage.cpp:104
#, kde-format
msgid ""
"When background shading is enabled, documents that have been viewed or "
"edited within the current session will have a shaded background. The most "
"recent documents have the strongest shade."
msgstr ""
"Kada je sijenčenje pozadine uključeno, dokumenti koje ste pogledali ili "
"uređivali u okviru tekuće sesije dobiće obojenu pozadinu. Najskorije viđeni "
"dokumenti imaju najizraženiju sijenku."

#: katefiletreeconfigpage.cpp:107
#, kde-format
msgid "Set the color for shading viewed documents."
msgstr "Postavi boju za sijenčenje pogledanih dokumenata."

#: katefiletreeconfigpage.cpp:109
#, kde-format
msgid ""
"Set the color for modified documents. This color is blended into the color "
"for viewed files. The most recently edited documents get most of this color."
msgstr ""
"Postavi boju za izmijenjene dokumente, koja se stapa sa bojom pogledanih "
"fajlova. Najskorije uređivani dokumenti poprimaju najviše ove boje."

#: katefiletreeconfigpage.cpp:114
#, kde-format
msgid ""
"When enabled, in tree mode, top level folders will show up with their full "
"path rather than just the last folder name."
msgstr ""
"Kada je uključeno, u stablo režimu na najvišem nivou  će se pojaviti sa "
"svojom punom putanjom, a ne samo poslednje ime fascikle."

#: katefiletreeconfigpage.cpp:117
#, kde-format
msgid ""
"When enabled, a toolbar with actions like “Save” are displayed above the "
"list of documents."
msgstr ""

#: katefiletreeconfigpage.cpp:136 katefiletreeplugin.cpp:127
#: katefiletreeplugin.cpp:134
#, kde-format
msgid "Documents"
msgstr "Dokumenti"

#: katefiletreeconfigpage.cpp:141
#, kde-format
msgid "Configure Documents"
msgstr "Podesite dokumente"

#: katefiletreemodel.cpp:494
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Open With"
msgctxt ""
"Open here is a description, i.e. 'list of widgets that are open' not a verb"
msgid "Open Widgets"
msgstr "Otvori pomoću"

#: katefiletreemodel.cpp:644
#, kde-format
msgctxt "%1 is the full path"
msgid ""
"<p><b>%1</b></p><p>The document has been modified by another application.</p>"
msgstr "<p><b>%1</b></p><p>Dokument je bio izmijenjen drugim programom.</p>"

#: katefiletreeplugin.cpp:248
#, kde-format
msgid "Previous Document"
msgstr "Prethodni dokument"

#: katefiletreeplugin.cpp:254
#, kde-format
msgid "Next Document"
msgstr "Naredni dokument"

#: katefiletreeplugin.cpp:260
#, fuzzy, kde-format
#| msgid "&Show Active"
msgid "&Show Active Document"
msgstr "Prikaži &aktivno"

#: katefiletreeplugin.cpp:265
#, kde-format
msgid "Save"
msgstr ""

#: katefiletreeplugin.cpp:266
#, kde-format
msgid "Save the current document"
msgstr "Snimi trenutni dokument"

#: katefiletreeplugin.cpp:270
#, kde-format
msgid "Save As"
msgstr ""

#: katefiletreeplugin.cpp:271
#, fuzzy, kde-format
#| msgid "Save the current document"
msgid "Save the current document under a new name"
msgstr "Snimi trenutni dokument"

#. i18n: ectx: Menu (go)
#: ui.rc:7
#, kde-format
msgid "&Go"
msgstr ""

#~ msgid "&Other..."
#~ msgstr "&Drugi..."

#~ msgid "Delete file?"
#~ msgstr "Obrisati datoteku?"

#~ msgid "Rename file"
#~ msgstr "Preimenuj datoteku"

#~ msgid "New file name"
#~ msgstr "Ime nove datoteke"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Samir Ribić"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "samir.ribic@etf.unsa.ba"

#, fuzzy
#~| msgctxt "@action:inmenu"
#~| msgid "Close Other"
#~ msgid "Close Widget"
#~ msgstr "Zatvori druge"

#, fuzzy
#~| msgctxt "@action:inmenu"
#~| msgid "Copy Filename"
#~ msgctxt "@action:inmenu"
#~ msgid "Copy File Path"
#~ msgstr "Kopiraj ime datoteke"

#~ msgid "&View"
#~ msgstr "&Pregled"

#~ msgid "Save Current Document"
#~ msgstr "Snimi trenutni dokument"

#~ msgid "Application '%1' not found."
#~ msgstr "Program „%1“ nije nađen."

#~ msgid "Application not found"
#~ msgstr "Program nije nađen"
