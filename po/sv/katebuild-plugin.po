# translation of katebuild-plugin.po to Swedish
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2008, 2009, 2010.
# Stefan Asserhall <stefan.asserhall@bredband.net>, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2018, 2019, 2020, 2021, 2022, 2023.
# Arve Eriksson <031299870@telia.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: katebuild-plugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-18 02:00+0000\n"
"PO-Revision-Date: 2023-08-01 08:12+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.08.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Stefan Asserhäll,Arve Eriksson"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "stefan.asserhall@bredband.net,031299870@telia.com"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "Utmatning"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr "Bygg igen"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr "Avbryt"

#: buildconfig.cpp:26
#, kde-format
msgid "Add errors and warnings to Diagnostics"
msgstr "Lägg till fel och varningar i diagnostik"

#: buildconfig.cpp:37
#, kde-format
msgid "Build & Run"
msgstr "Bygg och kör"

#: buildconfig.cpp:43
#, kde-format
msgid "Build & Run Settings"
msgstr "Bygg och körinställningar"

#: plugin_katebuild.cpp:212 plugin_katebuild.cpp:219 plugin_katebuild.cpp:1221
#, kde-format
msgid "Build"
msgstr "Bygg"

#: plugin_katebuild.cpp:222
#, kde-format
msgid "Select Target..."
msgstr "Välj mål..."

#: plugin_katebuild.cpp:227
#, kde-format
msgid "Build Selected Target"
msgstr "Bygg markerat mål"

#: plugin_katebuild.cpp:232
#, kde-format
msgid "Build and Run Selected Target"
msgstr "Bygg och kör markerat mål"

#: plugin_katebuild.cpp:237
#, kde-format
msgid "Stop"
msgstr "Stoppa"

#: plugin_katebuild.cpp:242
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr "Fokusera på nästa flik till vänster"

#: plugin_katebuild.cpp:262
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr "Fokusera på nästa flik till höger"

#: plugin_katebuild.cpp:284
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "Målinställningar"

#: plugin_katebuild.cpp:403
#, kde-format
msgid "Build Information"
msgstr "Bygginformation"

#: plugin_katebuild.cpp:619
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "Det finns ingen fil eller katalog angiven att bygga."

#: plugin_katebuild.cpp:623
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr ""
"Filen \"%1\" är inte en lokal fil. Icke-lokala filer kan inte kompileras."

#: plugin_katebuild.cpp:670
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""
"Kan inte köra kommando: %1\n"
"Arbetssökvägen finns inte: %2"

#: plugin_katebuild.cpp:684
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "Misslyckades köra \"%1\". Avslutningsstatus = %2"

#: plugin_katebuild.cpp:699
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr "Byggning av <b>%1</b> avbruten"

#: plugin_katebuild.cpp:806
#, kde-format
msgid "No target available for building."
msgstr "Inget mål tillgängligt att bygga."

#: plugin_katebuild.cpp:820
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr "Det finns ingen lokal fil eller katalog angiven att bygga."

#: plugin_katebuild.cpp:826
#, kde-format
msgid "Already building..."
msgstr "Bygger redan..."

#: plugin_katebuild.cpp:853
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "Bygger mål <b>%1</b>..."

#: plugin_katebuild.cpp:867
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr "<title>Byggresultat:</title><nl/>%1"

#: plugin_katebuild.cpp:903
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr "Byggning <b>%1</b> klar. %2 fel, %3 varning(ar), %4 anmärkning(ar)"

#: plugin_katebuild.cpp:909
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "Hittade ett fel."
msgstr[1] "Hittade %1 fel."

#: plugin_katebuild.cpp:913
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "Hittade en varning."
msgstr[1] "Hittade %1 varningar."

#: plugin_katebuild.cpp:916
#, kde-format
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "Hittade ett anmärkning."
msgstr[1] "Hittade %1 anmärkningar."

#: plugin_katebuild.cpp:921
#, kde-format
msgid "Build failed."
msgstr "Byggning misslyckades."

#: plugin_katebuild.cpp:923
#, kde-format
msgid "Build completed without problems."
msgstr "Byggning avslutades utan problem."

#: plugin_katebuild.cpp:928
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr "Byggning <b>%1 avbruten</b>. %2 fel, %3 varning(ar), %4 anmärkning(ar)"

#: plugin_katebuild.cpp:952
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr "Kan inte köra: %1 Ingen arbetskatalog inställd."

#: plugin_katebuild.cpp:1178
#, kde-format
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "fel"

#: plugin_katebuild.cpp:1181
#, kde-format
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "varning"

#: plugin_katebuild.cpp:1184
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr "anmärkning|information"

#: plugin_katebuild.cpp:1187
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "odefinierad referens"

#: plugin_katebuild.cpp:1220 TargetModel.cpp:285 TargetModel.cpp:297
#, kde-format
msgid "Target Set"
msgstr "Måluppsättning"

#: plugin_katebuild.cpp:1222
#, kde-format
msgid "Clean"
msgstr "Rensa"

#: plugin_katebuild.cpp:1223
#, kde-format
msgid "Config"
msgstr "Config"

#: plugin_katebuild.cpp:1224
#, kde-format
msgid "ConfigClean"
msgstr "Rensa Config"

#: plugin_katebuild.cpp:1415
#, kde-format
msgid "Cannot save build targets in: %1"
msgstr ""

#: TargetHtmlDelegate.cpp:50
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr "<B>M:</B> %1"

#: TargetHtmlDelegate.cpp:52
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr "<B>Kat:</B> %1"

#: TargetHtmlDelegate.cpp:101
#, kde-format
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr ""
"Lämna tom för att använda katalogen för det aktuella dokumentet.\n"
"Lägg till sökkataloger genom att lägga till sökvägar åtskilda av ';'"

#: TargetHtmlDelegate.cpp:105
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"Använd:\n"
"\"%f\" för den aktuella filen\n"
"\"%d\" för den aktuella filens katalog\n"
"\"%n\" för aktuellt filnamn utan suffix."

#: TargetModel.cpp:530
#, kde-format
msgid "Project"
msgstr ""

#: TargetModel.cpp:530
#, kde-format
msgid "Session"
msgstr ""

#: TargetModel.cpp:624
#, kde-format
msgid "Command/Target-set Name"
msgstr "Kommando/Måluppsättningens namn"

#: TargetModel.cpp:627
#, kde-format
msgid "Working Directory / Command"
msgstr "Arbetskatalog/Kommando"

#: TargetModel.cpp:630
#, kde-format
msgid "Run Command"
msgstr "Kör kommando"

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""
"Filtrera mål, använd piltangenter för att markera, returtangent för att köra"

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr "Skapa ny måluppsättning"

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr "Kopiera kommando eller måluppsättning"

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr "Ta bort nuvarande mål eller måluppsättning"

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "Lägg till nytt mål"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr "Bygg markerat mål"

#: targets.cpp:48
#, kde-format
msgid "Build and run selected target"
msgstr "Bygg och kör markerat mål"

#: targets.cpp:52
#, kde-format
msgid "Move selected target up"
msgstr "Flytta markerat mål uppåt"

#: targets.cpp:56
#, kde-format
msgid "Move selected target down"
msgstr "Flytta markerat mål neråt"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "&Bygg"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr "Infoga sökväg"

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr "Välj katalog att infoga"

#~ msgid "Project Plugin Targets"
#~ msgstr "Mål projektinsticksprogram"

#~ msgid "build"
#~ msgstr "bygg"

#~ msgid "clean"
#~ msgstr "rensa"

#~ msgid "quick"
#~ msgstr "snabb"

#~ msgid "Building <b>%1</b> completed."
#~ msgstr "Byggning av <b>%1</b> färdig."

#~ msgid "Building <b>%1</b> had errors."
#~ msgstr "Byggning av <b>%1</b> hade fel."

#~ msgid "Building <b>%1</b> had warnings."
#~ msgstr "Byggning av <b>%1</b> hade varningar."

#~ msgid "Show:"
#~ msgstr "Visa:"

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "Fil"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "Rad"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "Meddelande"

#~ msgid "Next Error"
#~ msgstr "Nästa fel"

#~ msgid "Previous Error"
#~ msgstr "Föregående fel"

#~ msgid "Show Marks"
#~ msgstr "Visa markörer"

#~ msgctxt "@info"
#~ msgid ""
#~ "<title>Could not open file:</title><nl/>%1<br/>Try adding a search path "
#~ "to the working directory in the Target Settings"
#~ msgstr ""
#~ "<title>Kunde inte öppna fil:</title><nl/>%1<br/>Försök att lägga till en "
#~ "sökväg till arbetskatalogen i målinställningarna"

#~ msgid "Error"
#~ msgstr "Fel"

#~ msgid "Warning"
#~ msgstr "Varning"

#~ msgid "Only Errors"
#~ msgstr "Bara fel"

#~ msgid "Errors and Warnings"
#~ msgstr "Fel och varningar"

#~ msgid "Parsed Output"
#~ msgstr "Tolkad utmatning"

#~ msgid "Full Output"
#~ msgstr "Fullständig utmatning"

#~ msgid ""
#~ "Check the check-box to make the command the default for the target-set."
#~ msgstr ""
#~ "Markera kryssrutan för att välja förvalt kommando för måluppsättningen."

#~ msgid "Select active target set"
#~ msgstr "Välj aktiv måluppsättning"

#~ msgid "Filter targets"
#~ msgstr "Filtrera mål"

#~ msgid "Build Default Target"
#~ msgstr "Bygg standardmål"

#, fuzzy
#~| msgid "Build Default Target"
#~ msgid "Build and Run Default Target"
#~ msgstr "Bygg standardmål"

#~ msgid "Build Previous Target"
#~ msgstr "Bygg föregående mål"

#~ msgid "Active target-set:"
#~ msgstr "Aktiv måluppsättning:"

#~ msgid "config"
#~ msgstr "config"

#~ msgid "Kate Build Plugin"
#~ msgstr "Kate bygginsticksprogram"

#~ msgid "Select build target"
#~ msgstr "Välj byggmål"

#~ msgid "Filter"
#~ msgstr "Filter"

#~ msgid "Build Output"
#~ msgstr "Byggutmatning"

#~ msgctxt "@info"
#~ msgid "<title>Could not open file:</title><nl/>%1"
#~ msgstr "<title>Kunde inte öppna filen:</title><nl/>%1"

#~ msgid "Next Set of Targets"
#~ msgstr "Nästa måluppsättningar"

#~ msgid "No previous target to build."
#~ msgstr "Inget föregående mål att bygga."

#~ msgid "No target set as default target."
#~ msgstr "Inget mål inställt som standardmål."

#~ msgid "No target set as clean target."
#~ msgstr "Inget mål inställt som rent mål."

#~ msgid "Target \"%1\" not found for building."
#~ msgstr "Mål \"%1\" kunde inte hittas för att byggas."

#~ msgid "Really delete target %1?"
#~ msgstr "Verkligen ta bort mål %1?"

#~ msgid "Nothing built yet."
#~ msgstr "Ingenting byggt ännu."

#~ msgid "Target Set %1"
#~ msgstr "Måluppsättning %1"

#~ msgid "Target"
#~ msgstr "Mål"

#~ msgid "Target:"
#~ msgstr "Mål:"

#~ msgid "from"
#~ msgstr "från"

#~ msgid "Sets of Targets"
#~ msgstr "Måluppstättningar"

#~ msgid "Make Results"
#~ msgstr "Byggresultat"

#~ msgid "Others"
#~ msgstr "Övriga"

#~ msgid "Quick Compile"
#~ msgstr "Snabbkompilering"

#~ msgid "The custom command is empty."
#~ msgstr "Det egna kommandot är tomt."

#~ msgid "New"
#~ msgstr "Nytt"

#~ msgid "Copy"
#~ msgstr "Kopiera"

#~ msgid "Delete"
#~ msgstr "Ta bort"

#~ msgid "Quick compile"
#~ msgstr "Snabbkompilering"

#~ msgid "Run make"
#~ msgstr "Bygg"

#~ msgid "..."
#~ msgstr "..."

#~ msgid "Break"
#~ msgstr "Avbryt"

#~ msgid "There is no file to compile."
#~ msgstr "Det finns ingen fil att kompilera."
